tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})

  ;
  catch [RuntimeException ex] { System.err.println(ex.getMessage()); }

zakres
  : ^(EB {scope_id=enterScope();} (e+=expr | d+=decl | e+=zakres)* {scope_id=leaveScope();})
    -> block(wyr={$e}, dekl={$d}, s={scope_id.toString()})
  ;

decl  :
        ^(VAR i1=ID) {declareVariable($ID.text);} -> dek(n={$ID.text}, s={scope_id.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st}, p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st}, p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st}, p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) { Integer scope = findVariable($ID.text); } -> assign(n={$ID.text + "_" + scope.toString()}, p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID { Integer scope = findVariable($ID.text); } -> id(n={$ID.text}, s={scope.toString()})
    ;
